import {  LocalLoadPurchases } from "@/data/usecases";
import  { mockPurchases,CacheStoreSpy } from '@/data/tests';
 
type SutTypes= {
  sut: LocalLoadPurchases;
  cacheStore: CacheStoreSpy;
}

// Factory
const makeSut = (timestamp = new Date()): SutTypes => {
  const cacheStore = new CacheStoreSpy();      
  const sut = new LocalLoadPurchases(cacheStore,timestamp);   
  return {
    sut,
    cacheStore,
  }  
}

describe('LocalSavePurchases', () => {

  // não limpa o chace quando chamar a classe
  test('Should not delete or insert cache on sut.init', () => {
    // const cacheStore = new CacheStoreSpy();
    const { cacheStore } = makeSut();
      // vai injetar esse chace em algum component
      // new LocalSavePurchases(cacheStore);      
      // expect(cacheStore.deleteCallsCount).toBe(0);

      expect(cacheStore.actions).toEqual([]);
  });

  // foi colocado junto com o teste "Should insert new Cache if delete succeeds"
  // test('Should delete old cache on sut.save', async () => {
  //     // const cacheStore = new CacheStoreSpy();      
  //     // const sut = new LocalSavePurchases(cacheStore);        
  //     const { sut , cacheStore} = makeSut();
  //     await sut.save(mockPurchases())
  //     // expect(cacheStore.deleteCallsCount).toBe(1);
  //     expect(cacheStore.messages).toEqual([CacheStoreSpy.Message.delete,CacheStoreSpy.Message.insert]);
  //     expect(cacheStore.deleteKey).toBe('purchases');
  // });

  
  // test('Should call delete with correct key', async () => {
  //     // const cacheStore = new CacheStoreSpy();      
  //     // const sut = new LocalSavePurchases(cacheStore);        
  //     const { sut , cacheStore} = makeSut();
  //     await sut.save()
  //     // apaga o item com essa chave
  //     expect(cacheStore.key).toBe('purchases');
  // });
  
  test('Should not insert new Cache if delete fails', async  () => {      
      const { sut , cacheStore} = makeSut();
      
      // mockImplementationOnce, substitui a implementação
      // cria um helper só pra isso
      // jest.spyOn(cacheStore, 'delete').mockImplementationOnce( ()  => {
      //   throw new Error()
      // })
      cacheStore.simulateDeleteError()

       const promise = sut.save(mockPurchases());
      // sumula a falha do delinsertCallsCountete
      // expect(cacheStore.insertCallsCount).toBe(0);
      expect(cacheStore.actions).toEqual([CacheStoreSpy.Action.delete]);
      // rejects.toThrow, a classe não trata o error
      await expect(promise).rejects.toThrow();
  });

  // test('Should insert new Cache if delete succeeds', async () => {      
  //     const { sut , cacheStore} = makeSut();
  //      await sut.save(mockPurchases());
  //     expect(cacheStore.deleteCallsCount).toBe(1);
  //     expect(cacheStore.insertCallsCount).toBe(1);
  // });

  // test('Should insert new Cache if delete succeeds', async () => {      
  //     const { sut , cacheStore} = makeSut();
  //      await sut.save(mockPurchases());
  //     expect(cacheStore.deleteCallsCount).toBe(1);
  //     expect(cacheStore.insertCallsCount).toBe(1);
  //     expect(cacheStore.insertKey).toBe('purchases');
  // });

  test('Should insert new Cache if delete succeeds', async () => {      
      const { sut , cacheStore} = makeSut();

      const timestamp = new Date();
      const puchases = mockPurchases();

      const promise = await sut.save(puchases);

      // expect(cacheStore.deleteCallsCount).toBe(1);
      // expect(cacheStore.insertCallsCount).toBe(1);
      expect(cacheStore.actions).toEqual([CacheStoreSpy.Action.delete,CacheStoreSpy.Action.insert]);
      expect(cacheStore.deleteKey).toBe('purchases');
      expect(cacheStore.insertKey).toBe('purchases');
      // expect(cacheStore.insertValues).toEqual(puchases);
      expect(cacheStore.insertValues).toEqual({
        timestamp,
        value: puchases
      });

      // a promise tem que ser tratada
      // await expect(promise).resolves.toBeFalsy();
  });

  test('Should throw if insert throws', async () => {      
    const { sut , cacheStore} = makeSut();
    cacheStore.simulateInsertError();
    const promise = sut.save(mockPurchases());
    expect(cacheStore.actions).toEqual([CacheStoreSpy.Action.delete,CacheStoreSpy.Action.insert]);
    await expect(promise).rejects.toThrow();
});


})