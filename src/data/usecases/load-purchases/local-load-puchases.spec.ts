import {  LocalLoadPurchases } from "@/data/usecases";
import  { mockPurchases,CacheStoreSpy , getCacheEXpirationDate } from '@/data/tests';
 
type SutTypes= {
  sut: LocalLoadPurchases;
  cacheStore: CacheStoreSpy;
}
const makeSut = (timestamp = new Date()): SutTypes => {
  const cacheStore = new CacheStoreSpy();      
  const sut = new LocalLoadPurchases(cacheStore,timestamp);   
  return {
    sut,
    cacheStore,
  }  
}

describe('LocalSavePurchases', () => {
  test('Should not delete or insert cache on sut.init', () => {
    const { cacheStore } = makeSut();      
    expect(cacheStore.actions).toEqual([]);
  });

  // test('Should call correct key on load', async () => {
  //   const { cacheStore, sut } = makeSut();      
  //   await sut.loadAll();
  //   expect(cacheStore.actions).toEqual([CacheStoreSpy.Action.fetch]);
  //   expect(cacheStore.fetchKey).toBe('purchases');
  // });

  test('Should return emply list if fails', async () => {
    const { cacheStore, sut } = makeSut();      
    cacheStore.simulateFetchtError();
    const purchases = await sut.loadAll();
    expect(cacheStore.actions).toEqual([CacheStoreSpy.Action.fetch,CacheStoreSpy.Action.delete]);
    expect(cacheStore.deleteKey).toBe('purchases')
    expect(purchases).toEqual([]);
  });


  test('Should return a list of purchases if cache is valid', async () => {
    
    const currentDate = new Date();
    // const timestamp = new Date(currentDate);
    // timestamp.setDate(timestamp.getDate()-3);
    const timestamp = getCacheEXpirationDate(currentDate);
    timestamp.setSeconds(timestamp.getSeconds() + 1);

    const { cacheStore, sut } = makeSut(timestamp);

    cacheStore.fetchResult = {
      timestamp,
      value: mockPurchases()
    };

    const purchases = await sut.loadAll();
    expect(cacheStore.actions).toEqual([CacheStoreSpy.Action.fetch]);
    expect(cacheStore.fetchKey).toBe('purchases');
    expect(purchases).toEqual(cacheStore.fetchResult.value);

  });


  test('Should return an empty list if cache is expired', async () => {
    
    const currentDate = new Date();
    // const timestamp = new Date(currentDate);
    // timestamp.setDate(timestamp.getDate()-3);
    const timestamp = getCacheEXpirationDate(currentDate);
    timestamp.setSeconds(timestamp.getSeconds() - 1);

    const { cacheStore, sut } = makeSut(currentDate);

    cacheStore.fetchResult = {
      timestamp,
      value: mockPurchases()
    };

    const purchases = await sut.loadAll();
    expect(cacheStore.actions).toEqual([CacheStoreSpy.Action.fetch, CacheStoreSpy.Action.delete]);
    expect(cacheStore.fetchKey).toBe('purchases');
    expect(cacheStore.deleteKey).toBe('purchases');
    expect(purchases).toEqual([]);

  });

  test('Should return an empty list if cache is on expiration date', async () => {
    
    const currentDate = new Date();
    // const timestamp = new Date(currentDate);
    // timestamp.setDate(timestamp.getDate()-3);
    const timestamp = getCacheEXpirationDate(currentDate);

    const { cacheStore, sut } = makeSut(currentDate);

    cacheStore.fetchResult = {
      timestamp,
      value: mockPurchases()
    };

    const purchases = await sut.loadAll();
    expect(cacheStore.actions).toEqual([CacheStoreSpy.Action.fetch, CacheStoreSpy.Action.delete]);
    expect(cacheStore.fetchKey).toBe('purchases');
    expect(cacheStore.deleteKey).toBe('purchases');
    expect(purchases).toEqual([]);

  });

  test('Should return an empty list if cache is empty', async () => {
    
    const currentDate = new Date();
    // const timestamp = new Date(currentDate);
    // timestamp.setDate(timestamp.getDate()-3);
    const timestamp = getCacheEXpirationDate(currentDate);
    timestamp.setSeconds(timestamp.getSeconds() + 1);

    const { cacheStore, sut } = makeSut(timestamp);

    cacheStore.fetchResult = {
      timestamp,
      value: []
    };

    const purchases = await sut.loadAll();
    expect(cacheStore.actions).toEqual([CacheStoreSpy.Action.fetch]);
    expect(cacheStore.fetchKey).toBe('purchases');
    expect(purchases).toEqual([]);

  });
})