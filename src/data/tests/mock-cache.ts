import { CacheStore } from "@/data/protocols/cache";
import { SavePurchases, LoadPurchases } from "@/domain/usecases";

/*
 não usa pro de produção, pois é uma boa prática colocar no teste diferente.
 para lembrar de muda nele
*/
const maxAgeInDays =  3;

export const getCacheEXpirationDate = (timestamp:Date):Date => {
  const maxCacheAge = new Date(timestamp);
  maxCacheAge.setDate(maxCacheAge.getDate() - maxAgeInDays );
  return maxCacheAge;
}

export class CacheStoreSpy implements CacheStore  {
  //controla a ordem das chamadas
  actions: Array<CacheStoreSpy.Action> = [];
  // deleteCallsCount = 0;
  // insertCallsCount = 0;
  deleteKey: string = '';
  insertKey: string = '';
  fetchKey: string = '';
  insertValues: Array<SavePurchases.Params> = [];
  fetchResult:  any;

  fetch (key:string): any {
    this.actions.push(CacheStoreSpy.Action.fetch);
    // this.deleteCallsCount++;
    this.fetchKey = key;

    return this.fetchResult;
  }

  delete (key:string): void {
    this.actions.push(CacheStoreSpy.Action.delete);
    // this.deleteCallsCount++;
    this.deleteKey = key;
  }

  insert (key:string, value: any): void {
    this.actions.push(CacheStoreSpy.Action.insert);
    // this.insertCallsCount++;
    this.insertKey = key;
    this.insertValues = value;
  }

  replace (key:string, value: any): void {
    this.delete(key);
    this.insert(key, value);
  }

  simulateDeleteError () : void {
    jest.spyOn(CacheStoreSpy.prototype, 'delete').mockImplementationOnce( ()  => {
      this.actions.push(CacheStoreSpy.Action.delete);
      throw new Error()
    })
  }
  
  simulateInsertError () : void {
    jest.spyOn(CacheStoreSpy.prototype, 'insert').mockImplementationOnce( ()  => {
      this.actions.push(CacheStoreSpy.Action.insert);
      throw new Error()
    })
  }

  simulateFetchtError () : void {
    jest.spyOn(CacheStoreSpy.prototype, 'fetch').mockImplementationOnce( ()  => {
      this.actions.push(CacheStoreSpy.Action.fetch);
      throw new Error()
    })
  }
}

//controla a ordem das chamadas
export namespace CacheStoreSpy  {
  export enum Action  {
    delete,
    insert,
    fetch
  }
}